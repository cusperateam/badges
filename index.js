const AWS = require('aws-sdk');
const fetch = require("node-fetch");
const { createCanvas, loadImage } = require('canvas')
const config = require("./config.json")

AWS.config.update({
    region: "us-east-2"
});

const s3 = new AWS.S3({ params: { Bucket: config.AWS_BUCKET } });

async function getResponse(id) {
    return fetch(config.CONFIG_URL + id, {
        method: 'get'
    }).then(function (response) {
        return response && response.json();
    })
}

async function upload(prod, fileName, fileData) {
    // AWS.config.update({ accessKeyId: "ACCESS_KEY", secretAccessKey: 'SECRET_KEY' });
    const params = {
        Key: "badges/" + prod + "/" + "latest/" + fileName,
        Body: fileData,
        ACL: 'public-read',
        CacheControl: 'no-cache'
    };

    return await s3.upload(params, function (err, data) {
        if (err) {
            throw err;
        }
        console.log(`File uploaded successfully. ${data.Location}`);
        return data.Location;
    });
}

async function functionName(id) {
    var response;
    try {
        response = await getResponse(id);
        if (!response) {
            console.log("Failed for prod Id: " + id);
            return
        }
    } catch (e) {
        console.log("Failed for prod Id: " + id);
        return;
    }
    const width = 480;
    const height = 240;
    const canvas = createCanvas(width, height)
    const context = canvas.getContext('2d')

    const textAreaH = 70;
    const starW = 52;
    const starH = 49;
    var starX = 30;
    const starY = height - starH - textAreaH - 18;
    const imgH = height - textAreaH;

    const peerScore = Math.round(response.peerViewScore * 10) / 10 == 4 ? "4.0" : Math.round(response.peerViewScore * 10) / 10;
    const evidenceCount = response.evidenceCount;

    const text1 = peerScore + '/5 '
    const text2 = '(' + evidenceCount + ' reviews)'
    const myimg = await loadImage('./Review Badge_Body.png');

    const fullStar = await loadImage('./Star-Full.png')
    const halfStar = peerScore == 4 ? await loadImage('./Star_Empty.png') :
        peerScore > 4.5 ? fullStar : await loadImage('./Star_Half.png');
    context.drawImage(myimg, 0, 0, width, imgH)
    // + 9 for gap bw stars
    context.drawImage(fullStar, starX, starY, starW, starH)
    context.drawImage(fullStar, (starX += starW + 9), starY, starW, starH)
    context.drawImage(fullStar, (starX += starW + 9), starY, starW, starH)
    context.drawImage(fullStar, (starX += starW + 9), starY, starW, starH)
    context.drawImage(halfStar, (starX += starW + 9), starY, starW, starH)

    context.strokeStyle = "#1A1A1A";
    context.font = '800 80px Arial';
    context.fillStyle = "#EEB417";
    context.lineWidth = 3;
    context.fillText(peerScore, ((width - 115) + (115 - context.measureText(peerScore).width) / 2), 110);
    context.strokeText(peerScore, ((width - 115) + (115 - context.measureText(peerScore).width) / 2), 110);
    context.fill();
    context.stroke();

    context.font = '250 50px IBM Plex Sans'
    context.fillStyle = "#1F2349"
    const padding = (width - context.measureText(text1 + text2).width) / 2;
    context.fillText(text1, padding, height - 10, width);
    context.fillStyle = "#4a4d4f"
    context.fillText(text2, padding + context.measureText(text1).width, height - 10, width);

    const byteData = await canvas.toBuffer('image/png', { compressionLevel: 3, filters: canvas.PNG_FILTER_NONE });

    const outputFileName = `badge.png`
    upload(id, outputFileName, byteData);
}

function storeImages() {
    // const prods = []
    const prods = ["sms-gateway-center-x-2111", "6connex-x-14457", "remo-conference-x-10038", "teamzy-crm-x-16116", "meetup-pro-x-1996", "price2spy-x-1074", "demandmatrix-x-13094", "rategain-x-16918", "streamz-x-3688", "engagebay-marketing-x-13048", "phonexa-x-6257", "marvia-x-8888", "intentwise-x-17370", "sprighub-x-17238", "activecampaign-x-1105", "delivra-x-5600", "makesbridge-x-436", "resci-x-6069", "leadsquared-marketing-automation-x-1256", "jumplead-x-1128", "emfluence-marketing-platform-x-5565", "mission-suite-x-10454"]
    for (let id of prods) {
        functionName(id);
    }
}

module.exports = storeImages();